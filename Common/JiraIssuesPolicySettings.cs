﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.TeamFoundation.VersionControl.Client;
using System.Text.RegularExpressions;
using System.Runtime.Serialization;

using Spartez.TFS4JIRA.CheckInPolicy.PolicyHelper;
using Spartez.TFS4JIRA.CheckInPolicy.JIRA;
using EasySec.Encryption;

using System.Security.Cryptography;

namespace Spartez.TFS4JIRA.CheckInPolicy.Settings
{  
    [Serializable]
    public class JiraIssuesPolicySettings
    {
        public JiraIssuesPolicySettings()
        {
            JiraURL = "";
            Login = "";
            _encryptedPassword = "";
        }

        [NonSerialized] 
        private static DPAPIEncryptor encryptor = new DPAPIEncryptor();

        private string _encryptedPassword;
        public string JiraURL { get; set; }
        public string Login { get; set; }
        public string Password 
        {
            get { return (_encryptedPassword == "" || _encryptedPassword == null) ? "" : encryptor.Decrypt(_encryptedPassword); }
            set { _encryptedPassword = (value == "" || value == null) ? "" : encryptor.Encrypt(value); }
        }

        public JiraConnectionSettings toJiraConnectionSettings()
        {
            return new JiraConnectionSettings(JiraURL, Login, Password);
        }

        public void updateFromJiraConnectionSettings(JiraConnectionSettings settingsModel)
        {
            JiraURL = settingsModel.JiraURL;
            Login = settingsModel.Login;
            Password = settingsModel.Password;
        }
    }   
}
