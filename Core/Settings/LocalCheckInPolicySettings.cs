﻿using System;
using Spartez.TFS4JIRA.CheckInPolicy.JIRA;
using EasySec.Encryption;

namespace Spartez.TFS4JIRA.CheckInPolicy.JIRA
{  
    [Serializable]
    public class LocalCheckInPolicySettings
    {
        public LocalCheckInPolicySettings(string jiraUrl)
        {
            JiraUrl = jiraUrl;
            Login = "";
            _encryptedPassword = "";
        }


        public LocalCheckInPolicySettings()
        {
            JiraUrl = "";
            Login = "";
            _encryptedPassword = "";
        }

        [NonSerialized] 
        private static readonly DPAPIEncryptor Encryptor = new DPAPIEncryptor();

        private string _encryptedPassword;
        public string JiraUrl { get; set; }
        public string Login { get; set; }
        public string Password 
        {
            get { return String.IsNullOrEmpty(_encryptedPassword) ? String.Empty : Encryptor.Decrypt(_encryptedPassword); }
            set { _encryptedPassword = String.IsNullOrEmpty(value) ? String.Empty : Encryptor.Encrypt(value); }
        }
    }   
}
