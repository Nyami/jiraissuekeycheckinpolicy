﻿using System;
using System.Text;
using System.Net;

namespace Spartez.TFS4JIRA.CheckInPolicy.JIRA
{
    public class JiraConnection
    {
        LocalCheckInPolicySettings localCheckInPolicySettings;

        public JiraConnection(LocalCheckInPolicySettings localCheckInPolicySettings)
        {
            this.localCheckInPolicySettings = localCheckInPolicySettings;
        }

        public string TestConnection()
        {
            try 
            {
                StringOp("rest/api/2/myself");
                return null;
            }
            catch(UriFormatException exception)
            {
                return "JIRA URL format is invalid. Example URL: https://example.atlassian.net";
            }
            catch (Exception)
            {
                return "Could not connect to JIRA. Please check used protocol (HTTP/HTTPS), JIRA URL, login and password.";
            }
        }

        public bool CheckIfIssueExists(string issueKey)
        {            
            try
            {
                StringOp("rest/api/2/issue/" + issueKey);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private void StringOp(string restApi)
        {
            Uri baseUri = new Uri(localCheckInPolicySettings.JiraUrl);
            Uri uri = new Uri(baseUri, restApi);

            var request = WebRequest.Create(uri);

            string authenticationString = localCheckInPolicySettings.Login + ":" + localCheckInPolicySettings.Password;
            string encodedAuthenticationString = Convert.ToBase64String(Encoding.Default.GetBytes(authenticationString));
            request.Headers["Authorization"] = "Basic " + encodedAuthenticationString;

            request.ContentType = "application/json";
            request.Method = "GET";

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();

            if (response.StatusCode != HttpStatusCode.OK)
            {
                throw new Exception("Server returned status code: " + response.StatusCode);
            }            
        }
    }
}
